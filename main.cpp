#include <iostream>

#include "BraunRobinson.h"

int main() {

    std::cout << "Input:" << std::endl;
    std::cout << matrix_C[0][0] << " " << matrix_C[0][1] << " " << matrix_C[0][2] << std::endl;
    std::cout << matrix_C[1][0] << " " << matrix_C[1][1] << " " << matrix_C[1][2] << std::endl;
    std::cout << matrix_C[2][0] << " " << matrix_C[2][1] << " " << matrix_C[2][2] << std::endl;

    alg_dopolnenie(matrix_C,  reverse_matrix_C);
    transport_matrix_of_alg_dop(reverse_matrix_C,  determinate(matrix_C));

    cost_of_game_v = analitic_method(reverse_matrix_C, optimal_strategy_x, optimal_strategy_y);

    std::cout << "Analitic method:" << std::endl;
    std::cout << "x* = " << optimal_strategy_x[0][0] << " " << optimal_strategy_x[0][1] << " " << optimal_strategy_x[0][2] << std::endl;
    std::cout << "y* = " << optimal_strategy_y[0][0] << " " << optimal_strategy_y[1][0] << " " << optimal_strategy_y[2][0] << std::endl;
    std::cout << "v = " << cost_of_game_v << std::endl;

    std::cout << "Braun-Robinson method:" << std::endl;
    braun_robinson_method(matrix_C);

    return 0;
}
