#ifndef TI_LR1_BRAUNROBINSON_H
#define TI_LR1_BRAUNROBINSON_H

#include <vector>

// вариант 6
std::vector<std::vector<double>> matrix_C = { { 6.0, 18.0, 6.0 }, { 17.0, 8.0, 18.0 }, { 16.0, 10.0, 10.0 } };
std::vector<std::vector<double>> reverse_matrix_C = { { 0.0, 0.0, 0.0 }, { 0.0, 0.0, 0.0 }, { 0.0, 0.0, 0.0 } };

std::vector<std::vector<double>> optimal_strategy_x = { { 0.0, 0.0, 0.0 } };;
std::vector<std::vector<double>> optimal_strategy_y = { { 0.0 }, { 0.0 }, { 0.0 } };
double cost_of_game_v = 0.0;

double determinate(std::vector<std::vector<double>> &input_m);
void alg_dopolnenie(std::vector<std::vector<double>> &input_m, std::vector<std::vector<double>> &output_m);
void transport_matrix_of_alg_dop(std::vector<std::vector<double>> &input_m, double det);

double analitic_method(std::vector<std::vector<double>> &input_m,
                       std::vector<std::vector<double>> &opt_x,
                       std::vector<std::vector<double>> &opt_y);

void braun_robinson_method(std::vector<std::vector<double>> &input_m);

#endif //TI_LR1_BRAUNROBINSON_H
