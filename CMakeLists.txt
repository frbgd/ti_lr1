cmake_minimum_required(VERSION 3.16)
project(ti_lr1)

set(CMAKE_CXX_STANDARD 14)

add_executable(ti_lr1 main.cpp BraunRobinson.h BraunRobinson.cpp)