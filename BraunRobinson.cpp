#include <algorithm>
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <cmath>

// вычисление определителя
double determinate(std::vector<std::vector<double>> &input_m) {
    double det = 0.0;
    det += input_m[0][0] * input_m[1][1] * input_m[2][2];
    det += input_m[0][1] * input_m[1][2] * input_m[2][0];
    det += input_m[0][2] * input_m[1][0] * input_m[2][1];
    det -= input_m[0][2] * input_m[1][1] * input_m[2][0];
    det -= input_m[0][0] * input_m[1][2] * input_m[2][1];
    det -= input_m[0][1] * input_m[1][0] * input_m[2][2];
    return det;
}

// вычисление алгебраических дополнений
void alg_dopolnenie(std::vector<std::vector<double>> &input_m, std::vector<std::vector<double>> &output_m) {
    output_m[0][0] = input_m[1][1] * input_m[2][2] - input_m[2][1] * input_m[1][2];
    output_m[0][1] = -(input_m[1][0] * input_m[2][2] - input_m[2][0] * input_m[1][2]);
    output_m[0][2] = input_m[1][0] * input_m[2][1] - input_m[2][0] * input_m[1][1];
    output_m[1][0] = -(input_m[0][1] * input_m[2][2] - input_m[2][1] * input_m[0][2]);
    output_m[1][1] = input_m[0][0] * input_m[2][2] - input_m[2][0] * input_m[0][2];
    output_m[1][2] = -(input_m[0][0] * input_m[2][1] - input_m[2][0] * input_m[0][1]);
    output_m[2][0] = input_m[0][1] * input_m[1][2] - input_m[1][1] * input_m[0][2];
    output_m[2][1] = -(input_m[0][0] * input_m[1][2] - input_m[1][0] * input_m[0][2]);
    output_m[2][2] = input_m[0][0] * input_m[1][1] - input_m[1][0] * input_m[0][1];
}

// транспортирование матрицы
void transport_matrix_of_alg_dop(std::vector<std::vector<double>> &input_m, double det) {
    double temp = 0.0;
    temp = input_m[0][1];
    input_m[0][1] = input_m[1][0];
    input_m[1][0] = temp;
    temp = input_m[0][2];
    input_m[0][2] = input_m[2][0];
    input_m[2][0] = temp;
    temp = input_m[1][2];
    input_m[1][2] = input_m[2][1];
    input_m[2][1] = temp;

    for (size_t index_row = 0; index_row < 3; ++index_row) {
        for (size_t index_column = 0; index_column < 3; ++index_column) {
            input_m[index_row][index_column] *= 1.0 / det;
        }
    }
}

// расчёт аналитическим методом
double analitic_method(std::vector<std::vector<double>> &input_m,
                       std::vector<std::vector<double>> &opt_x,
                       std::vector<std::vector<double>> &opt_y) {
    //расчёт оптимальной стартегии х*= (u*C^(-1))/(u*C^(-1)*u^T), где u=(1,1,1), u^T-транспонированный вектор u
    // temp - значение u*C^(-1)*u^T
    //расчёт оптимальной стартегии y*= (C^(-1)*u^T)/(u*C^(-1)*u^T), где u=(1,1,1), u^T-транспонированный вектор u
    //v=1/(u*C^(-1)*u^T)

    double temp = 0.0;
    //u*C^(-1)
    opt_x[0][0] = input_m[0][0] + input_m[1][0] + input_m[2][0];
    opt_x[0][1] = input_m[0][1] + input_m[1][1] + input_m[2][1];
    opt_x[0][2] = input_m[0][2] + input_m[1][2] + input_m[2][2];
    temp = opt_x[0][0] + opt_x[0][1] + opt_x[0][2];//u*C^(-1)*u^T
    for (size_t index_column = 0; index_column < 3; ++index_column) {
        opt_x[0][index_column] *= 1.0 / temp; //вектор x*
    }
    //C^(-1)*u^T
    opt_y[0][0] = input_m[0][0] + input_m[0][1] + input_m[0][2];
    opt_y[1][0] = input_m[1][0] + input_m[1][1] + input_m[1][2];
    opt_y[2][0] = input_m[2][0] + input_m[2][1] + input_m[2][2];
    for (size_t index_row = 0; index_row < 3; ++index_row) {
        opt_y[index_row][0] *= 1.0 / temp; //вектор y*
    }
    //возвращаем цену игры
    return 1.0 / temp;
}

//алгоритм Брауна-Робинсона
void braun_robinson_method(std::vector<std::vector<double>> &input_m) {
    std::fstream result_file("table.csv", std::ios_base::out | std::ios_base::trunc);

    std::vector<int> wins_of_A = { 0,0,0 };         // выигрыш игрока А
    std::vector<int> loses_of_B = { 0,0,0 };        // проигрыш игрока В

    std::vector<double> high_cost_of_game;          // средние значения верхней цены игры
    std::vector<double>::iterator iterator_of_high_cost_of_game;
    std::vector<double> low_cost_of_game;           // средние значения нижней цены игры
    std::vector<double>::iterator iterator_of_low_cost_of_game;
    double step_count = 1.0;                        // количество шагов
    int pos_min_element_of_high_cost_of_game = 0;   // позиция минимального элемента в массиве средних значений верхней цены игры
    int pos_max_element_of_low_cost_of_game = 0;    // позиция максимального элемента в массиве средних значений верхней цены игры

    // количество использованных стратегий игроками А и В
    int count_of_x1 = 1;
    int count_of_x2 = 0;
    int count_of_x3 = 0;
    int count_of_y1 = 1;
    int count_of_y2 = 0;
    int count_of_y3 = 0;

    // выбор стратегии х1 игроком А на шаге 1
    wins_of_A[0] = input_m[0][0];
    wins_of_A[1] = input_m[1][0];
    wins_of_A[2] = input_m[2][0];
    // выбор стратегии у1 игроком В на шаге 1
    loses_of_B[0] = input_m[0][0];
    loses_of_B[1] = input_m[0][1];
    loses_of_B[2] = input_m[0][2];

    // нахождение максимального выиграша у игрока А
    std::vector<int>::iterator max_result_of_A;
    max_result_of_A = std::max_element(wins_of_A.begin(), wins_of_A.end());
    int pos_max_win_of_A = std::distance(wins_of_A.begin(), max_result_of_A);
    // нахождение минимального проигрыша у игрока В
    std::vector<int>::iterator min_result_of_B;
    min_result_of_B = std::min_element(loses_of_B.begin(), loses_of_B.end());
    int pos_min_lose_of_B = std::distance(loses_of_B.begin(), min_result_of_B);

    // подсчёт среднего значения верхней цены игры
    high_cost_of_game.push_back(wins_of_A.at(pos_max_win_of_A) * 1.0 / step_count);
    // подсчёт среднего значения нижней цены игры
    low_cost_of_game.push_back(loses_of_B.at(pos_min_lose_of_B) * 1.0 / step_count);

    // вычисление значения ошибки
    double e_error = high_cost_of_game.at(0) - low_cost_of_game.at(0);
    std::vector<double> errors;
    errors.push_back(e_error);

    result_file << "k" << ";" << "выбор игрока" << ";" << "выигрыш игрока А" << ";"<<";" <<";"<< "проигрыш игрока В" << ";" <<";"<<";"<< "1 / k * v[k] - верх" <<";"<< "1/k*v[k]-ниж|" << ";" << "e-погрешность" << "\n";
    result_file << " " << ";" << "A|B" << ";" << "x1" << ";" << "x2" << ";" << "x3" << ";" << "y1" << ";" << "y2" << ";" << "y3" << "\n";

    do
    {
        if (step_count == 1)
        {
            result_file << step_count << ";" << "x1 y1" << ";" << wins_of_A[0] << ";" << wins_of_A[1] << ";" << wins_of_A[2] << ";" << loses_of_B[0] << ";" << loses_of_B[1] << ";" << loses_of_B[2] << ";" << floor(high_cost_of_game.back() * 100) / 100 << ";"<< floor(low_cost_of_game.back() * 100) / 100<<";" << floor(e_error * 100) / 100 << std::endl;
        }
        step_count++;
        result_file << step_count << ";";
        if (0 == pos_max_win_of_A)          // если максимальное значение будет при выборе стратегии х1
        {
            loses_of_B[0] += input_m[0][0];
            loses_of_B[1] += input_m[0][1];
            loses_of_B[2] += input_m[0][2];
            count_of_x1++;
            result_file << "x1 ";
        }
        else if (1 == pos_max_win_of_A)     // если максимальное значение будет при выборе стратегии х2
        {
            loses_of_B[0] += input_m[1][0];
            loses_of_B[1] += input_m[1][1];
            loses_of_B[2] += input_m[1][2];
            count_of_x2++;
            result_file << "x2 ";
        }
        else if (2 == pos_max_win_of_A)     // если максимальное значение будет при выборе стратегии х3
        {
            loses_of_B[0] += input_m[2][0];
            loses_of_B[1] += input_m[2][1];
            loses_of_B[2] += input_m[2][2];
            count_of_x3++;
            result_file << "x3 ";
        }

        if (0 == pos_min_lose_of_B)         // если минимальный проигрыш будет при выборе стратегии у1
        {
            wins_of_A[0] += input_m[0][0];
            wins_of_A[1] += input_m[1][0];
            wins_of_A[2] += input_m[2][0];
            count_of_y1++;
            result_file << "y1" << ";";
        }
        else if (1 == pos_min_lose_of_B)    // если минимальный проигрыш будет при выборе стратегии у2
        {
            wins_of_A[0] += input_m[0][1];
            wins_of_A[1] += input_m[1][1];
            wins_of_A[2] += input_m[2][1];
            count_of_y2++;
            result_file << "y2" << ";";
        }
        else if (2 == pos_min_lose_of_B)    // если минимальный проигрыш будет при выборе стратегии у3
        {
            wins_of_A[0] += input_m[0][2];
            wins_of_A[1] += input_m[1][2];
            wins_of_A[2] += input_m[2][2];
            count_of_y3++;
            result_file << "y3" << ";";
        }
        result_file << wins_of_A[0] << ";" << wins_of_A[1] << ";" << wins_of_A[2] << ";" << loses_of_B[0] << ";" << loses_of_B[1] << ";" << loses_of_B[2] << ";" ;

        // нахождение максимального выигрыша у игрока А
        max_result_of_A = std::max_element(wins_of_A.begin(), wins_of_A.end());
        pos_max_win_of_A = std::distance(wins_of_A.begin(), max_result_of_A);

        // нахождение минимального проигрыша у игрока В
        min_result_of_B = std::min_element(loses_of_B.begin(), loses_of_B.end());
        pos_min_lose_of_B = std::distance(loses_of_B.begin(), min_result_of_B);

        // подсчёт среднего значения верхней цены игры
        high_cost_of_game.push_back(wins_of_A.at(pos_max_win_of_A) * 1.0 / step_count);

        // подсчёт среднего значения нижней цены игры
        low_cost_of_game.push_back(loses_of_B.at(pos_min_lose_of_B) * 1.0 / step_count);

        // поиск минимального элемента среди значений верхней цены игры
        iterator_of_high_cost_of_game = std::min_element(high_cost_of_game.begin(), high_cost_of_game.end());
        pos_min_element_of_high_cost_of_game = std::distance(high_cost_of_game.begin(), iterator_of_high_cost_of_game);

        // поиск максимального элемента среди значений верхней цены игры
        iterator_of_low_cost_of_game = std::max_element(low_cost_of_game.begin(), low_cost_of_game.end());
        pos_max_element_of_low_cost_of_game = std::distance(low_cost_of_game.begin(), iterator_of_low_cost_of_game);

        // вычисление величины ошибки
        e_error = high_cost_of_game.at(pos_min_element_of_high_cost_of_game) - low_cost_of_game.at(pos_max_element_of_low_cost_of_game);
        result_file <<  floor(high_cost_of_game.back() * 100) / 100 << ";" << floor(low_cost_of_game.back() * 100) / 100 << ";" << floor(e_error * 100) / 100 << std::endl;
        errors.push_back(e_error);

    } while (e_error > 0.1);

    result_file.close();

    std::vector<double>::iterator max_result_of_cost;
    max_result_of_cost = std::max_element(high_cost_of_game.begin(), high_cost_of_game.end());
    int pos_max_cost_of_game = std::distance(high_cost_of_game.begin(), max_result_of_cost);

    std::vector<double>::iterator min_result_of_cost;
    min_result_of_cost = std::min_element(low_cost_of_game.begin(), low_cost_of_game.end());
    int pos_min_cost_of_game = std::distance(low_cost_of_game.begin(), min_result_of_cost);
    std::cout << *std::min_element(high_cost_of_game.begin(), high_cost_of_game.end()) << " " << *std::max_element(low_cost_of_game.begin(), low_cost_of_game.end()) << std::endl;

    std::cout << "Error value (e) & step count (k):" << std::endl;
    std::cout << "e = " << e_error << std::endl;
    std::cout << "k = " << step_count << std::endl;
    std::cout << "Output:" << std::endl;
    std::cout << "x*[" << step_count << "]=(" << count_of_x1 << "/" << step_count << "=" << double(count_of_x1 / step_count) << "," << count_of_x2 << "/" << step_count << "=" << double(count_of_x2 / step_count) << "," << count_of_x3 << "/" << step_count << "=" << double(count_of_x3 / step_count) << ")" << std::endl;
    std::cout << "y*[" << step_count << "]=(" << count_of_y1 << "/" << step_count << "=" << double(count_of_y1 / step_count) << "," << count_of_y2 << "/" << step_count << "=" << double(count_of_y2 / step_count) << "," << count_of_y3 << "/" << step_count << "=" << double(count_of_y3 / step_count) << ")" << std::endl;
}
